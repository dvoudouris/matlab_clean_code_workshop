function c = add_numbers(a, b)
    % (C) Copyright 2023 Remi Gau developers
    c = a + b;
end
